# Growave/Client

Simple [Growave API](https://growave.io/docs/api) client in PHP


## Requirements

* PHP 5.3+ with [cURL support](http://php.net/manual/en/book.curl.php).

How To Use
----------

#### Include the class file ####

```php
require_once('SSW/Client.php');
```

#### Get access token ####

```php
$client = new SSW\Client("YOUR_SSW_API_KEY", "YOUR_SSW_API_SECRET");
$client->getAppToken('read_user write_user read_wishlist write_wishlist read_review write_review read_gallery write_reward app_install');
```

GET Request Example
-------------------

Get user's wishlist

```php
$userId = 1;
$userWishlist = $client->getWishlist($user_id);
```
